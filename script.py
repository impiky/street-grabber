import os
import time
import csv
import configparser
import pickle
import os.path
import requests
import lxml
import transliterate

from random import randint
from datetime import datetime

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

from bs4 import BeautifulSoup

from transliterate import get_translit_function

from openpyxl import Workbook


VMCONFIG_FILE_PATH = r'C:\\abc\\ABCMOBI.ini'
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SPREADSHEET_ID = '1JROmkidlIUVouQA9WEsde987YeI_naw7PNptTBxduNI'
RANGE_NAME = 'A:D'


headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
    'cache-control': 'max-age=0',
    'cookie': '_bl_uid=9sjpmu577v3y2vf1C6vvdbOr3m06; ali_apache_id=11.227.32.219.1552635780771.337254.8; _uab_collina=155263578198465978226986; cna=JbQIFR7UdEMCAXPSsBreksOi; _ym_uid=1552635784261975291; _ym_d=1552635784; _ga=GA1.2.546548176.1552635786; aep_common_f=eUGdo3aI+ZPqMd/s3zNvtjWG6lmcnWvbpSDyE7UPmWMLfOGJeu5j0A==; UM_distinctid=16a4387e3661f-0d852ac44b262-e323069-144000-16a4387e367965; ali_beacon_id=11.227.32.219.1552635780771.337254.8; __utma=3375712.546548176.1552635786.1555911140.1558324838.8; __utmz=3375712.1558324838.8.3.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; _bl_uid=42jnbwLgbn8t4kitp4a7ydmkLzOp; intl_locale=ru_RU; _gid=GA1.2.762058347.1560136146; _ym_isad=2; _mle_tmp0=iiCGajxLJhPRfqiVFROq8pAR%2BXoi3hZkVZBIqez1peKP63opMrz2hPhXKUT2lZyl17G%2FP2j%2FQFFisZtS7hKBp0RLfbac6vbx8ygHRIHnWqZL8uqE%2Ft%2FIImrQxtmGczo9; acs_usuc_t=acs_rt=a825d66d264a47649eea17ef7cd1d77a&x_csrf=7l0n0d_z992o; JSESSIONID=575B241D6E98A57218AF83741FD01F4A; _mle_tmp_harden0=COMyBPKnGxIg1PC5i8Kdr%2BGdJRVsZUo3suoHbpNHNe3fofox8LT7Ks%2Fm8IJ%2BjlJs%2Baw8AYprtP%2Bzc6Tyo%2FkKb7LhUKAWJQLBvq2E7wCidaj8Jt4%2BKZEjzQQMX2%2F5APbA; aep_history=keywords%5E%0Akeywords%09%0A%0Aproduct_selloffer%5E%0Aproduct_selloffer%0932994758118%0932967083468%091000005799445%0932964838103%0932958526739%0932834934629%0932682855945%0932877997595; _mle_tmp_enc0=Ey%2Fp8LswzxA3J47VsqxI%2BzjfFGmtd%2FOquIlf07UvhlHxeAtbyPIkNGUP3ftVFrg6qh16QsjDgNnN2mwa0Q2%2F0WjxMnhMILAcLMnsmyBvo%2F48QnpIGHLhINHr6DyG0moC; _ym_visorc_29739640=b; _m_h5_tk=6ab0720efe1d4cc6fea37c8383df358e_1560157157897; _m_h5_tk_enc=8825c418b3ec87bf4692058ada31ec73; _hvn_login=13; xman_us_t=x_lid=cn254321384avqae&sign=y&x_user=XXB+5LdiTeyYYIjOSLW/T9pumf/8S1EkTL44AyfaDGQ=&ctoken=bxmwq0p9ryrc&need_popup=y&l_source=aliexpress; aep_usuc_t=ber_l=A0; xman_f=5qmZPxaGGlZXN1IdgrGMrWjw4g/T6B8u43zoG/w6MZXQz2DjVgOOFpl2XIQJ6SufDxQtNOUb86TGj5fDQudPqeTMSxLQlifZvjsh91byjW2iI8gIiAisUXHaeGM/+1zjox8HdLDmyr9q0fX9ZJCPqedKO3LiMfKvPkabNccB/kZd7ByzK+Akj1lMQKvTDDeM9eMU6yGcy4KbVO3cGEjPpw2W0BiyncEsOfD8/Zr4yXkffJBRH5r+2BgYX5y4HlYhgnXFSFDFmOwvaeMNW6Jeyp6M6PdRGkdYMvGPmhhoskreamkszafzzM4UFdgIWbk92YfjRkVnCA9RgvLXxCo+DRzceUcW5B8KUl6jXsQSutsXBsafPvoILFVEhaVHbdI+/4cqNfajMeRPFrK3pq92tFm8RV60cMbTlwy1pyQL5Pc=; xman_us_f=zero_order=y&x_locale=ru_RU&x_l=1&last_popup_time=1553848394904&x_user=CN|l|sy|ifm|1854421384&no_popup_today=n; aep_usuc_f=site=rus&aep_cet=mobile&province=917477670000000000&city=917477679070000000&c_tp=USD&x_alimid=1854421384&isb=y&s_locale=ru_RU&region=RU&b_locale=ru_RU; ali_apache_tracktmp=W_signed=Y; ali_apache_track=mt=1|ms=|mid=cn254321384avqae; intl_common_forever=3VaBh49oilnGk7uK59MO+b29Ygjf6btR6G2mThy3/xJPgMwq9o1FBg==; l=bBNp8ojHvmdLGlgSBOCaIuI8Lu7OSIRYSuPRwCYMi_5C91Ts9G_Ole-poe96Vj5RsFLB4s6vWje9-etki; isg=BPn5kqVtgRZV1F333FVUgwO6CGUTruy8K-vGoxsufiCfohk0Y1eDib_wJObxH4Xw; xman_t=WMrx3r68XuZGtk7neFXotuPObmt5MBA1JzpualzP1C61bN73ZjcSNvdRFtlv0Ny/eHyjs3JdxxHud65YET1JAHzXW6iV5S+ns5p7tEvWFT+DAdbOqGxAbBn5dO8RDna8UcPfqzIL4GJRFR+QKfCnZGBavM7q4098yJMLNAwQdbyHRuuzLgDa++2OqCL8nDUhzNrlVfXx0JicUI6k6VoZk+MVv6AyUtNR9CTqomKq8rFPKhWypQlw1wESsl1oGE242Bqs/xlQgAlhRtR9cQNHy3/gfsf1hNrpocg2xHIumo8z5VpMk/bjGXuh5sIaWcsO7pVE99p2qYpkVXpiOpkKBaOKHL4U0cCQ+f5pmxFrzmUbgsqIWdSuXtbYBarcAmuGyWScuLKh5dXFt0BCACpjmEfUQiqiUzWDEaejfw8FiNBl6V8A89fDSx4JPEoaaIiw81E3wnX0U+D8F8prIkQbS19JfQnbE5bCSe3NvaARsnfSqNoSi5tYnPyzN5rwJr+qVbB1Y5/iXsurN0PKGXAB53ST4/mAEQ71Rq2d2p7qwmeTR5v02cioAJj8CsmDrpCBEQub8NdJEMcdWdnnD00Hf+n3CATQ7AlLE5+w/cADmWs+umNzOuZulKrwcS0fePzkbnuJZARRbgU=',
    'referer': 'https://ru.aliexpress.com/',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3729.131 Safari/537.36'
}


def getConfig(section, key, configfile):
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
yzmuser = getConfig("vcode", "username", "config.ini")
yzmpassword = getConfig("vcode", "password", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini") # for 32bit system delete x86 from path
# vmid = getConfig("VMInfo", "VMID", VMCONFIG_FILE_PATH)
# internet_login = getConfig("internet", "login", "config.ini")
# internet_pwd = getConfig("internet", "password", "config.ini")


def get_streets_and_houses(headers):
        data = []
        url_num = 1
        count = 1
        while url_num != 33:
           if url_num == 8 or url_num == 12 or url_num == 29:
               url_num += 1
               continue
           url = "https://ato.by/streets/letter/{}".format(url_num)
           res = requests.get(url, headers=headers).text
           soup = BeautifulSoup(res, 'lxml')
           streets_table_left = soup.find("div" , attrs={"style" : "float: left; width: 350px;"})
           for li_item in streets_table_left.find_all("li"):
            #    print(li_items.find("a")["href"])
               street_url1 = requests.get("https://ato.by{}".format(li_item.find("a")["href"]),  headers=headers).text
               soup = BeautifulSoup(street_url1, 'lxml')
               house_numbers1 = soup.find("select", attrs={"id" : "hSHouseId"})
               for house in house_numbers1.find_all("option"):
                   if "— дома (0)—" in house.text:
                    #    print("No houses!")
                       data.append([li_item.text, " "])
                       break
                   elif "— дома" in house.text:
                       continue
                   else:
                       data.append([li_item.text, house.text])
           url = "https://ato.by/streets/letter/{}".format(url_num)
           res = requests.get(url, headers=headers).text   
           soup = BeautifulSoup(res, 'lxml')         
           streets_table_right = soup.find("div" , attrs={"style" : "float: right; width: 350px;"})
           for li_item in streets_table_right.find_all("li"):
            #    print(li_items.find("a")["href"])
               street_url2 = requests.get("https://ato.by{}".format(li_item.find("a")["href"]),  headers=headers).text
               soup = BeautifulSoup(street_url2, 'lxml')
               house_numbers2 = soup.find("select", attrs={"id" : "hSHouseId"})
               for house in house_numbers2.find_all("option"):
                   if "— дома (0)—" in house.text:
                        # print("No houses!")
                        data.append([li_item.text, " "])
                        break
                   elif "— дома" in house.text:
                        continue
                   else:
                        data.append([li_item.text, house.text])
           url_num += 1
           print(data)
        return data


def get_data_from_sheets(scopes, spreadsheet_id, range_name):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    cell_number = 1
    post_index = []
    state = []
    district = []
    city = []
    translit_ru = get_translit_function("ru")
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'client_secret.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=spreadsheet_id,
                                range=range_name).execute()
    values = result.get('values', [])
    

    if not values:
        print('No data found.')
    else:
        # print('Name, Major:')
        for row in values:
            if "Индекс" in row[0]:
                continue
            post_index.append(row[0])
            state.append(row[1])
            district.append(row[2])
            city.append(row[3])
    return post_index, state, district, city


def write_to_excel(data, post_index, state, district, city):
    street = []
    house_number = []
    translit_ru = get_translit_function("ru")
    # workbook = xlsxwriter.Workbook('out.xlsx')
    # worksheet = workbook.add_worksheet()
    # writer = pd.ExcelWriter('out.xlsx')
    wb = Workbook()
    ws = wb.active
    exl = 1
    for i in range(0, len(data)):
        try:
            street_and_house_num = data.pop(i)
            if street_and_house_num[1] == " ":
                street_number = randint(1, 123)
                if street_number < 50:
                    street_and_house_num[1] = street_number
                else:
                    street_and_house_num[1] = str(street_number) + '/A'
            street.append(street_and_house_num[0])
            house_number.append(street_and_house_num[1])
            # sheet1.write(post_index[i], 0, i)
            # sheet1.write(state[i], 1, i)
            # sheet1.write(district[i], 2, i)
            # sheet1.write(city[i], 3, i)
            # sheet1.write(street[i], 4, i)
            # sheet1.write(house_number[i], 5, i)
            # df = pd.DataFrame({'Index' : post_index, 'State' : state, 'District' : district, 'City' : city, 'Street' : street, 'House number': house_number})
            # df = df.transpose()
            # df.to_excel(writer, 'Sheet1')
            # writer.save()
            # worksheet.write ("A1", "test")
            rand_i = randint(1, 2620)
            ws["A{}".format(exl)] = post_index[rand_i]
            ws["B{}".format(exl)] = state[1]
            ws["C{}".format(exl)] = district[1]
            ws["D{}".format(exl)] = city[1]
            ws["E{}".format(exl)] = street[i]
            ws["F{}".format(exl)] = house_number[i]
            # time.sleep(1)
            exl += 1
            # print(street)
            # print("--------------------")
            # print(house_number)
        except:
            pass
    
    wb.save('out.xlsx')


result = get_streets_and_houses(headers)
p_index, state, district, city = get_data_from_sheets(SCOPES, SPREADSHEET_ID, RANGE_NAME)
write_to_excel(result, p_index, state, district, city)